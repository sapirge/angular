import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  error='';
  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }
  login() {
    this.authService.login(this.email, this.password)
    .then(value => {
      this.router.navigate(['/']);//אחרי התחברות מעבר לדף של הטודוס
    })
    .catch(err => {
      this.error = err;
      console.log(err);
    })
  }

}