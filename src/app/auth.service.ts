import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';//טעינה של אות סרויס
import { Observable } from 'rxjs';//בשביל להסתיר את הכפתור לוג אאוט שהיוזר לא מחובר, שימוש Pipe sync
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';//על מנת להציג לכל יוזר את הטודוס שלו

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;//צריך אימפורט אובסרבובל בשביל להפעיל את זה
  constructor(private firebaseAuth: AngularFireAuth,private db:AngularFireDatabase){
    this.user = firebaseAuth.authState;
  }

  signUp(email: string, password: string) {//מקבל ערכים
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password); //פונקציה בפיירבייס של יצירת יוזר
  }
updateProfile(user, name:string) {
  user.updateProfile({displayName: name, photoURL: ''});
}
login(email: string, password: string) {//מקבלת אימייל וסיסמא 
  return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);//מעביר לפיירבייס 
}
logOut() {
  return this.firebaseAuth.auth.signOut();
}
addUser(user, name: string) {
  let uid = user.uid;
  let ref = this.db.database.ref('/'); // the main endpoint of the db
  ref.child('users').child(uid).push({'name':name}); // if the child doesn't exist - he creates it
}
}