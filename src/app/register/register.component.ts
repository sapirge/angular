import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  name: string;
  password: string;
  error="";

  constructor(private authService:AuthService, private router:Router){}

    signUp(){
        this.authService.signUp(this.email,this.password)//אחרי הרשמה שם משתמש יתעדכן ליוזר
        .then( value =>{
          this.authService.updateProfile(value.user,this.name);
          this.authService.addUser(value.user, this.name);//אחרי שנרשמים היוזר מתווסף לדטה בייס
        }).then(value =>{//אחרי דאן מופיע האירוע החד פעמי - פרומיס כאן זה מעבר לטודוס
          this.router.navigate(['/'])//אחרי שעושים הרשמה זה עובר לדף הראשי טודוס
        }).catch(err =>{
          this.error=err;
          console.log(err);
        }) 
      }


  ngOnInit() {
  }


 
 
}
