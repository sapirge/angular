import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';//מערכת ניווט לעמודים
import { AngularFireModule } from '@angular/fire';//התקנת פיירביסס
import { AngularFireDatabaseModule } from '@angular/fire/database';//התקנת פיירבייס
import { AngularFireAuthModule } from '@angular/fire/auth';//התקנת פיירבייס
import {environment} from '../environments/environment';//קישור לפיירבייס נמצא בקובץ אינרמנט
import { FormsModule } from '@angular/forms'//בשביל ליצור קשר עם המשתמש- שדה חיפוש שם משתמש ויציג את הטודודס
import {MatCardModule} from '@angular/material/card';//התקנה של העיצוב
import {MatInputModule} from '@angular/material/input';//התקנה של עיצוב לשדות אימפוט של סיסמא ומייל
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CodesComponent } from './codes/codes.component';
import { UsertodosComponent } from './usertodos/usertodos.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    TodoComponent,
    LoginComponent,
    RegisterComponent,
    CodesComponent,
    UsertodosComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    MatInputModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AngularFireModule.initializeApp(environment.firebase),   
    RouterModule.forRoot([
      {path:'', component:TodosComponent},
      {path:'register', component:RegisterComponent},
      {path:'login', component:LoginComponent},
      {path:'codes', component:CodesComponent},
      {path:'usertodos', component:UsertodosComponent},
      {path:'**', component:TodosComponent}
    ])
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
