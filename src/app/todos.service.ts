import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private db:AngularFireDatabase,private authService:AuthService) { }

  addTodos(todo: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/todos').push({'text': todo});
      }
    )
  }
  deleteTodo(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').remove(key);
      }
    )
  }
  update(key, text) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').update(key,{'text':text});
      }
    )
  }
 

}
