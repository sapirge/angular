import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';//שימוש בדטה בייס 

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {
user = "sapir";
show;
todos= [
]


constructor(private db:AngularFireDatabase) { }

  ngOnInit() {//זה קורה כאשר טוענים את הדף ולכן עשינו עוד פונקציה יוזר ציינג
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(//ניגש ליוזרס תחת שם היוזר ותחת הטודוס שלו ועובר על האובייקטים שלו
    todos => {
    this.todos = [];//איפוס המערך טודוס
    todos.forEach(//רץ בלולאה על כל האובייקטים טודוס
    todo=>{//משתנה זמני טודו
    let y =todo.payload.toJSON();//פיילוד זה התוכן שזה טודוס (לקנות חלב וכאלה..
    y['$Key'] = todo.key;//הוספת שדה למערך Y של הקי
    this.todos.push(y);
    }
    )
    }
    )
    }

    changeUser() {
      this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe( // tell me for all the data changes on the things below
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y['$key'] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )
    }
   

}
